m4_changecom(`{{}}')m4_dnl
m4_dnl
m4_dnl Reference library for O3 FAD project
m4_dnl
m4_define(`_O3_URL',`file://home/hilaire/DEA/O3/web')m4_dnl
m4_dnl
m4_define(`_UPDATE',`<em>Updated on m4_esyscmd(date -u "+%Y/%m/%d at %T - %Z")</em>')m4_dnl
m4_dnl
m4_define(`_HTML_START',`<html>
<head>
<meta name="Editor" content="M4 powered - Emacs"/>
<meta name="Author" content="Etudiant O3/DEA IHM-IE"/>
<meta name="Description" content="Cours sur FAD,$2"/>
<title>$1</title>
</head>

<body text="#000000" bgcolor="#ffffff" marginheight="0"
marginwidth="0" topmargin="0" leftmargin="0"/>
<center><font size+=4>FORMATION A DISTANCE</font></center>')m4_dnl
m4_dnl
m4_define(`_HTML_END',`<center><i><font face="lucida,arial" size=-2>_UPDATE</font></i></center>
</body>
</html>')m4_dnl
m4_dnl the 1st two parameter determine the page we are on
m4_dnl The 1st parameter is major task number
m4_dnl The 2nd parameter is the minor task number
m4_dnl The 3rd parameter is the content of the main page
m4_dnl The definition are: 
m4_dnl 0.0 for General Accueil
m4_dnl 1.0 Accueil activit� 1
m4_dnl 1.1 Notes sur la FAD
m4_dnl 1.2 Formulaires
m4_dnl 1.3 Site web
m4_dnl 1.4 6 caracteristiques cl�s
m4_define(`_GENERAL_LAYOUT',`<table cellspacing="0" cellpadding="0" width="100%">
<tr nosave>
<td align="left" valign="top" nowrap width="160pix">
Les liens
<br>
m4_ifelse($1,00,`_A00_ON'<br>,`_A00_OFF<br>')
m4_ifelse($1,10,`_A10_ON<br>
_A11_OFF<br>
_A12_OFF<br>
_A13_OFF<br>
_A14_OFF<br>',`_A10_OFF<br>')
m4_ifelse($1,11,`_A11_ON<br>_A12_OFF<br>_A13_OFF<br>_A14_OFF<br>')
m4_ifelse($1,12,`_A11_OFF<br>_A12_ON<br>_A13_OFF<br>_A14_OFF<br>')
m4_ifelse($1,13,`_A11_OFF<br>_A12_OFF<br>_A13_ON<br>_A14_OFF<br>')
m4_ifelse($1,13,`_A11_OFF<br>_A12_OFF<br>_A13_OFF<br>_A14_ON<br>')
</td>
<td>
$2
</td>
<p>
</table>
<hr>')m4_dnl
m4_dnl Defined Task for de navigation bar
m4_define(_A00_ON,_VIEWED(ACCUEIL))m4_dnl
m4_define(_A00_OFF,_LINK_TO_UNVIEWED(index.html,ACCUEIL))m4_dnl
m4_define(_A10_OFF,_LINK_TO_UNVIEWED(a10.html,ACTIVIT� 1))m4_dnl
m4_define(_A10_ON,_VIEWED(ACTIVIT� 1))m4_dnl
m4_define(_A11_ON,_TAB_SPC _VIEWED(Notes sur la FAD))m4_dnl
m4_define(_A11_OFF,_TAB_SPC _LINK_TO_UNVIEWED(a11.html,Notes sur la FAD))m4_dnl
m4_define(_A12_ON,_TAB_SPC _VIEWED(Formulaire))m4_dnl
m4_define(_A12_OFF,_TAB_SPC _LINK_TO_UNVIEWED(a12.html,Formulaire))m4_dnl
m4_define(_A13_ON,_TAB_SPC _VIEWED(Sites WEB))m4_dnl
m4_define(_A13_OFF,_TAB_SPC _LINK_TO_UNVIEWED(a13.html,Sites Web))m4_dnl
m4_define(_A14_ON,_TAB_SPC _VIEWED(6 Caracteristiques Cl�s))m4_dnl
m4_define(_A14_OFF,_TAB_SPC _LINK_TO_UNVIEWED(a14.html,6 Caract�ristiques Cl�s))m4_dnl
m4_dnl
m4_define(_VIEWED,<b>$1</b>)m4_dnl
m4_define(_TAB_SPC,`&nbsp;&nbsp;&nbsp;&nbsp;')m4_dnl
m4_define(`_LINK_TO_UNVIEWED',`<a href="_O3_URL/$1">$2</a>')m4_dnl
m4_define(`_LINK_TO_LABEL',`<a href="_O3_URL/$1">$2</a>')m4_dnl